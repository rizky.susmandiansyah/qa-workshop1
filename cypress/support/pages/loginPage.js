const loginLocator = require ('../locator/loginPageLocator');

class loginPage{
    async visit(url){
        cy.visit(url)
    }

    async inputUsername(username){
        return cy.get(loginLocator.loginLocator.inputUsername).type(username)
    }

    async inputPassword(password){
        return cy.get(loginLocator.loginLocator.inputPassword).type(password)
    }
    async buttonLogin(){
        cy.get(loginLocator.loginLocator.loginButton).click()
    }

}

module.exports = loginPage;
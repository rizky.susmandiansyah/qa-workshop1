module.exports={
    loginLocator:{
        inputUsername: '[data-test="username"]',
        inputPassword: '[data-test="password"]',
        loginButton: '[data-test="login-button"]'
    }
}